-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 06, 2022 at 09:15 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qlsv`
--

-- --------------------------------------------------------

--
-- Table structure for table `dmkhoa`
--

CREATE TABLE `DMKHOA` (
  `MAKH` varchar(6) NOT NULL,
  `TenKhoa` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `DMKHOA`
--

INSERT INTO `DMKHOA` (`MAKH`, `TenKhoa`) VALUES
('CNTT', 'Công nghệ thông tin'),
('VL', 'Vật Lý');
-- --------------------------------------------------------

--
-- Table structure for table `sinhvien`
--

CREATE TABLE `SINHVIEN` (
  `MaSV` varchar(6) NOT NULL,
  `HoSV` varchar(30) NOT NULL,
  `TenSV` varchar(15) NOT NULL,
  `GioiTinh` char(1) NOT NULL,
  `NgaySinh` datetime NOT NULL,
  `NoiSinh` varchar(50) NOT NULL,
  `DiaChi` varchar(50) NOT NULL,
  `MaKH` varchar(6) NOT NULL,
  `HocBong` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;

--
-- Dumping data for table `SINHVIEN`
--

INSERT INTO `SINHVIEN` (`MaSV`, `HoSV`, `TenSV`, `GioiTinh`, `NgaySinh`, `NoiSinh`, `DiaChi`, `MaKH`, `HocBong`) VALUES
('SV001', 'Nguyễn', 'Văn A', '1', '2000-01-01 00:00:00', 'Hà Nội', 'Hà Nội', 'CNTT', 1000000),
('SV002', 'Nguyễn', 'Văn B', '1', '2000-02-01 00:00:00', 'Hải Phòng', 'Hà Nội', 'CNTT', 4000000),
('SV003', 'Nguyễn', 'Văn C', '1', '2000-03-01 00:00:00', 'Tp. Hồ Chí Minh', 'Hà Nội', 'CNTT', 2000000),
('SV004', 'Nguyễn', 'Văn D', '1', '2000-04-01 00:00:00', 'Nghệ An', 'Hà Nội', 'VL', 1000000),
('SV005', 'Nguyễn', 'Văn E', '1', '2000-05-01 00:00:00', 'Quy Nhơn', 'Hà Nội', 'CNTT', 3000000),
('SV006', 'Nguyễn', 'Văn F', '1', '2000-06-01 00:00:00', 'Huế', 'Hà Nội', 'CNTT', 1000000),
('SV007', 'Nguyễn', 'Văn G', '1', '2000-07-01 00:00:00', 'Đà Nẵng', 'Hà Nội', 'CNTT', 2000000),
('SV008', 'Nguyễn', 'Văn H', '1', '2000-08-01 00:00:00', 'Bắc Ninh', 'Hà Nội', 'VL', 4000000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `DMKHOA`
--

ALTER TABLE `DMKHOA`
  ADD PRIMARY KEY (`MAKH`);

--
-- Indexes for table `SINHVIEN`
--

ALTER TABLE `SINHVIEN`
  ADD PRIMARY KEY (`MaSV`),
  ADD KEY `FK_SINHVIEN_DMKHOA` (`MaKH`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/*taskToday*/
SELECT *
FROM DMKHOA, SINHVIEN
WHERE DMKHOA.MAKH = SINHVIEN.MaKH AND DMKHOA.TenKhoa = 'Công nghệ thông tin'